use super::ruleset::Ruleset;
use super::ruleset::Algorithms;

// #[test]
// fn test_new_ruleset() {
//     let rules_string = "Foo".to_string();
//     let ruleset = Ruleset::new(Algorithms::BPF, rules_string);
//
//     assert_eq!(&ruleset.algorithm, &Algorithms::BPF);
//     //Enums only exists in one place in memory. So you can compare the references to an enum since they should point to the same place.
// }
// 
// #[test]
// fn test_set_algorithm() {
//     let rules_string = "Foo".to_string();
//     let mut ruleset = Ruleset::new(Algorithms::BPF, rules_string);
//
//     ruleset.algorithm = Algorithms::ParallelBloomFilters;
//
//     assert_eq!(&ruleset.algorithm, &Algorithms::ParallelBloomFilters);
// }
//
// #[test]
// fn test_set_rules() {
//     let rules_string = "Foo".to_string();
//     let mut ruleset = Ruleset::new(Algorithms::BPF, rules_string);
//
//     let new_string = "Bar".to_string();
//
//     ruleset.rules = new_string.clone();
//
//     assert_eq!(new_string, ruleset.rules.clone())
// }
