pub struct InspectionResult {
    pub results: Vec<usize>,
}

impl InspectionResult {
    pub fn new(size: usize) -> InspectionResult {
        if size != 0 {
            InspectionResult {
                results: vec![0; size],
            }
        }
        else {
            InspectionResult {
                results: Vec::new(),
            }
        }
    }
}
