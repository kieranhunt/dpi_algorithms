pub struct Ruleset {
    pub algorithm: Algorithms,
    pub rules: String, //This may change.
}

#[derive(PartialEq, Debug)]
pub enum Algorithms {
    Naive,
    RabinKarp,
    KnuthMorrisPratt,
    BoyerMoore,
    Bitap,
    Horspool,
    AhoCorasick,
    BloomFilter,
    Cuckoo,
}

impl Ruleset {
    pub fn new(algorithm: Algorithms, rules: String) -> Ruleset{
        Ruleset {
            algorithm: algorithm,
            rules: rules,
        }
    }
}
