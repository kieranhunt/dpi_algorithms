use uuid::Uuid;

#[derive(Clone)]
pub struct Data {
    id: String,
}

impl Data {
    pub fn new() -> Data{
        Data{
            id: Uuid::new_v4().to_simple_string(),
        }
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn set_id(&mut self, id: String){
        self.id = id;
    }
}
