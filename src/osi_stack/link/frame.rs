use osi_stack::network::packet::Packet;
use uuid::Uuid;

pub struct Frame {
    id: String,
    packet: Packet,
}

impl Frame {
    pub fn new(packet: Packet) -> Frame {
        Frame{
            id: Uuid::new_v4().to_simple_string(),
            packet: packet,
        }
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn set_id(&mut self, id: String){
        self.id = id;
    }

    pub fn get_packet(&self) -> &Packet {
        &self.packet
    }

    pub fn set_packet(&mut self, packet: Packet){
        self.packet = packet;
    }
}
