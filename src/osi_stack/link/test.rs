use osi_stack::application::data::Data;
use osi_stack::transport::segment::Segment;
use osi_stack::network::packet::Packet;

use super::frame::Frame;

#[test]
fn test_new_frame(){
    let data = Data::new();
    let segment = Segment::new(data);
    let packet = Packet::new(segment);
    let frame = Frame::new(packet);
}

#[test]
fn test_get_data_in_frame(){
    let data = Data::new();
    let data2 = data.clone();
    let segment = Segment::new(data);
    let packet = Packet::new(segment);
    let frame = Frame::new(packet);

    assert!(frame.get_packet().get_segment().get_data().get_id() == data2.get_id());
}

#[test]
fn test_set_packet_in_frame() {
    let data = Data::new();
    let segment = Segment::new(data);
    let packet = Packet::new(segment);
    let packet_clone = packet.clone();

    let data2 = Data::new();
    let segment2 = Segment::new(data2);
    let packet2 = Packet::new(segment2);
    let packet2_clone = packet2.clone();

    let mut frame = Frame::new(packet);
    frame.set_packet(packet2);

    assert!(frame.get_packet().get_id() != packet_clone.get_id());
    assert!(frame.get_packet().get_id() == packet2_clone.get_id());
}
