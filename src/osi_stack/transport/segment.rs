use osi_stack::application::data::Data;
use uuid::Uuid;

#[derive(Clone)]
pub struct Segment {
    id: String,
    data: Data,
}

impl Segment {
    pub fn new(data: Data) -> Segment{
        Segment{
            id: Uuid::new_v4().to_simple_string(),
            data: data,
        }
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn set_id(&mut self, id: String){
        self.id = id;
    }

    pub fn get_data(&self) -> &Data {
        &self.data
    }

    pub fn set_data(&mut self, data: Data){
        self.data = data;
    }
}
