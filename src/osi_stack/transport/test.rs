use osi_stack::application::data::Data;

use super::segment::Segment;

#[test]
fn test_new_segment() {
    let data = Data::new();
    let segment = Segment::new(data);
}

#[test]
fn test_data_in_segment() {
    let data = Data::new();
    let data_clone = data.clone();
    let segment = Segment::new(data);

    assert!(segment.get_data().get_id() == data_clone.get_id());
}

#[test]
fn test_set_data_in_segment() {
    let data = Data::new();
    let data_clone = data.clone();
    let data2 = Data::new();
    let data2_clone = data2.clone();

    let mut segment = Segment::new(data);
    segment.set_data(data2);

    assert!(segment.get_data().get_id() != data_clone.get_id());
    assert!(segment.get_data().get_id() == data2_clone.get_id());
}
