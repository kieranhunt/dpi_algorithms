use osi_stack::transport::segment::Segment;
use uuid::Uuid;

#[derive(Clone)]
pub struct Packet {
    id: String,
    segment: Segment,
}

impl Packet {
    pub fn new(segment: Segment) -> Packet {
        Packet {
            id: Uuid::new_v4().to_simple_string(),
            segment: segment,
        }
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn set_id(&mut self, id: String) {
        self.id = id;
    }

    pub fn get_segment(&self) -> &Segment {
        &self.segment
    }

    pub fn set_segment(&mut self, segment: Segment){
        self.segment = segment;
    }
}
