use osi_stack::application::data::Data;
use osi_stack::transport::segment::Segment;

use super::packet::Packet;

#[test]
fn test_new_packet(){
    let data = Data::new();
    let segment = Segment::new(data);
    let packet = Packet::new(segment);
}

#[test]
fn test_data_in_packet() {
    let data = Data::new();
    let data_clone = data.clone();
    let segment = Segment::new(data);
    let packet = Packet::new(segment);

    assert!(packet.get_segment().get_data().get_id() == data_clone.get_id());
}

#[test]
fn test_set_segment_in_packet() {
    let data = Data::new();
    let segment = Segment::new(data);
    let segment_clone = segment.clone();

    let data2 = Data::new();
    let segment2 = Segment::new(data2);
    let segment2_clone = segment2.clone();

    let mut packet = Packet::new(segment);
    packet.set_segment(segment2);

    assert!(packet.get_segment().get_id() != segment_clone.get_id());
    assert!(packet.get_segment().get_id() == segment2_clone.get_id());
}
