use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

const MAX_PATTERN_LENGTH: usize = 1000;

pub struct Bitap <'a> {
    pattern: &'a [u8],
}

impl <'a> Algorithm <'a> for Bitap <'a> {
    fn new(ruleset: &'a Ruleset) -> Bitap {
        Bitap {
            pattern: ruleset.rules.as_bytes(),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.pattern = ruleset.rules.as_bytes();
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let pattern_len = self.pattern.len();

        if pattern_len == 0 {
            inspection_result.results[0] = 1;
            return inspection_result;
        } else if packet_len == 0 {
            return inspection_result;
        }

        let mut R = [false; MAX_PATTERN_LENGTH];
        R[0] = true;

        for i in 0..packet_len {
            let mut k = pattern_len;
            loop {
                if k == 0 {
                    break;
                }
                R[k] = R[k-1] & (data[i] == self.pattern[k-1]);
                k = k - 1;
            }
            if R[pattern_len] {
                inspection_result.results[0] = 1;
                return inspection_result;
            }
        }
        inspection_result
    }
}

pub fn bitap(packet: &[u8], pattern_string: &String) -> bool {

    let pattern: &[u8] = pattern_string.as_bytes();

    let packet_len = packet.len();
    let pattern_len = pattern.len();

    if pattern_len == 0 {
        return true;
    } else if packet_len == 0 {
        return false;
    }

    let mut R = [false; MAX_PATTERN_LENGTH];
    R[0] = true;

    for i in 0..packet_len {
        let mut k = pattern_len;
        loop {
            if k == 0 {
                break;
            }
            R[k] = R[k-1] & (packet[i] == pattern[k-1]);
            k = k - 1;
        }
        if R[pattern_len] {
            return true;
        }
    }

    false
}
