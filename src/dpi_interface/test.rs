use dpi_interface::algorithm::Algorithm;
use dpi_interface::bpf::BPF;
use dpi_interface::naive::Naive;
use dpi_interface::rabinkarp::RabinKarp;
use dpi_interface::knuthmorrispratt::KnuthMorrisPratt;
use dpi_interface::boyermoore::BoyerMoore;
use dpi_interface::bitap::Bitap;
use dpi_interface::horspool::Horspool;
use dpi_interface::ahocorasick::AhoCorasick;
use dpi_interface::bloomfilter::BloomFilter;
use dpi_interface::cuckoofilter::cuckoofilter;
use dpi_interface::cuckoofilter::Cuckoo;
use pcap::Capture;
use ruleset::ruleset::Ruleset;
use ruleset::ruleset::Algorithms;
use std::path::Path;

#[test]
fn test_naive() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::Naive, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::Naive, "nareik".to_string());

    let mut naive = Naive::new(&ruleset);

    assert_eq!(naive.inspect_data(packet).results[0], 1);

    naive.change_ruleset(&ruleset2);
    assert_eq!(naive.inspect_data(packet).results[0], 0);
}

#[test]
fn test_rabin_karp() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::RabinKarp, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::RabinKarp, "nareik".to_string());

    let mut rabin_karp = RabinKarp::new(&ruleset);

    assert_eq!(rabin_karp.inspect_data(packet).results[0], 1);

    rabin_karp.change_ruleset(&ruleset2);
    assert_eq!(rabin_karp.inspect_data(packet).results[0], 0);
}

#[test]
fn test_knuth_morris_pratt() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::KnuthMorrisPratt, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::KnuthMorrisPratt, "iek".to_string());

    let mut knuth_morris_pratt = KnuthMorrisPratt::new(&ruleset);

    assert_eq!(knuth_morris_pratt.inspect_data(packet).results[0], 1);

    knuth_morris_pratt.change_ruleset(&ruleset2);
    assert_eq!(knuth_morris_pratt.inspect_data(packet).results[0], 0);
}

#[test]
fn test_boyer_mooore() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::BoyerMoore, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::BoyerMoore, "iek".to_string());
    let ruleset3 = Ruleset::new(Algorithms::BoyerMoore, "kieran".to_string());

    let mut boyer_moore = BoyerMoore::new(&ruleset);

    assert_eq!(boyer_moore.inspect_data(packet).results[0], 1);

    boyer_moore.change_ruleset(&ruleset2);
    assert_eq!(boyer_moore.inspect_data(packet).results[0], 0);

    boyer_moore.change_ruleset(&ruleset3);
    assert_eq!(boyer_moore.inspect_data(packet).results[0], 1);

}

#[test]
fn test_bitap() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::KnuthMorrisPratt, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::KnuthMorrisPratt, "iek".to_string());

    let mut bitap = Bitap::new(&ruleset);

    assert_eq!(bitap.inspect_data(packet).results[0], 1);

    bitap.change_ruleset(&ruleset2);
    assert_eq!(bitap.inspect_data(packet).results[0], 0);
}

#[test]
fn test_horspool() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::KnuthMorrisPratt, "er".to_string());
    let ruleset2 = Ruleset::new(Algorithms::KnuthMorrisPratt, "iek".to_string());

    let mut horspool = Horspool::new(&ruleset);

    assert_eq!(horspool.inspect_data(packet).results[0], 1);

    horspool.change_ruleset(&ruleset2);
    assert_eq!(horspool.inspect_data(packet).results[0], 0);
}

#[test]
fn test_aho_corasick() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::AhoCorasick, "ran;kieran;".to_string());
    let ruleset2 = Ruleset::new(Algorithms::AhoCorasick, "iek;".to_string());

    let mut aho_corasick: AhoCorasick = Algorithm::new(&ruleset);

    assert_eq!(aho_corasick.inspect_data(packet).results, vec![1,1]);

    aho_corasick.change_ruleset(&ruleset2);
    assert_eq!(aho_corasick.inspect_data(packet).results, vec![0]);
}

#[test]
fn test_bloom_filter() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::BloomFilter, "kieran;ran".to_string());
    let ruleset2 = Ruleset::new(Algorithms::BloomFilter, "aaaaaa;".to_string());

    let mut bloom_filter = BloomFilter::new(&ruleset);

    assert_eq!(bloom_filter.inspect_data(packet).results[0], 1);

    bloom_filter.change_ruleset(&ruleset2);
    assert_eq!(bloom_filter.inspect_data(packet).results[0], 0);
}

#[test]
fn test_cuckoo_filter() {
    let packet: &[u8] = &[0x6b, 0x69, 0x65, 0x72, 0x61, 0x6e];

    let ruleset = Ruleset::new(Algorithms::BloomFilter, "kieran;ran".to_string());
    let ruleset2 = Ruleset::new(Algorithms::BloomFilter, "aaaaaa;".to_string());

    let mut cuckoo_filter = Cuckoo::new(&ruleset);

    assert_eq!(cuckoo_filter.inspect_data(packet).results[0], 1);

    cuckoo_filter.change_ruleset(&ruleset2);
    assert_eq!(cuckoo_filter.inspect_data(packet).results[0], 0);
}
