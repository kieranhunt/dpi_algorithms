use bloomfilter::Bloom;
use std::str;
use crypto::digest::Digest;
use crypto::md5::Md5;
use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

pub struct BloomFilter {
    bloom_filter: Bloom,
}

impl BloomFilter {
    fn make_filter(patterns_vec: Vec<&[u8]>) -> Bloom {

        let mut size = patterns_vec.len();
        if size <= 0 {
            size = 1;
        }

        let mut bloom = Bloom::new(size*1000, size);


        for pattern in patterns_vec {
            bloom.set(pattern);
        }
        bloom
    }
    fn get_patterns(pattern: &[u8]) -> Vec<&[u8]> {

        const DELIMETER: u8 = 0x3b; //Delimeter is ';'

        let pattern_len = pattern.len();

        let mut patterns = Vec::new();

        let mut i = 0;

        for mut j in 0..pattern_len {
            if (pattern[j] == DELIMETER) {
                patterns.push(&pattern[i .. j]);
                j = j + 1;
                i = j;
            }
        }

        patterns
    }
}

impl <'a> Algorithm <'a> for BloomFilter {
    fn new(ruleset: &'a Ruleset) -> BloomFilter {
        BloomFilter {
            bloom_filter: BloomFilter::make_filter(BloomFilter::get_patterns(ruleset.rules.as_bytes())),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.bloom_filter = BloomFilter::make_filter(BloomFilter::get_patterns(ruleset.rules.as_bytes()));
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let packet = data;
        let packet_len = data.len();

        let mut md5 = Md5::new();

        let mut inspection_result = InspectionResult::new(1);

        for i in 0..packet_len {
            for j in i..packet_len {
                let packet_substring = &packet[i..j+1];
                if self.bloom_filter.check(packet_substring) {
                        inspection_result.results[0] = 1;
                        return inspection_result;
                }
            }
        }
        inspection_result
    }
}

// pub fn bloomfilter(packet: &[u8], pattern_string: &String) -> bool {
//     let patterns: &[u8] = pattern_string.as_bytes();
//     let patterns_vec = get_patterns(patterns);
//     let patterns_vec_len = patterns_vec.len();
//     let mut bloom_filter = BloomFilter::new(patterns_vec_len*1000, 100);
//
//     let packet_len = packet.len();
//
//     let mut md5 = Md5::new();
//
//     for pattern in patterns_vec {
//         md5.input(pattern);
//         bloom_filter.insert(&md5.result_str());
//         md5.reset();
//     }
//
//     for i in 0..packet_len {
//         for j in i..packet_len {
//             let packet_substring = &packet[i..j+1];
//             md5.input(packet_substring);
//             if bloom_filter.check(&md5.result_str()) {
//                     return true;
//             }
//             md5.reset();
//         }
//     }
//
//     return false;
// }
//
// fn get_patterns(pattern: &[u8]) -> Vec<&[u8]> {
//
//     const DELIMETER: u8 = 0x3b; //Delimeter is ';'
//
//     let pattern_len = pattern.len();
//
//     let mut patterns = Vec::new();
//
//     let mut i = 0;
//
//     for mut j in 0..pattern_len {
//         if (pattern[j] == DELIMETER) {
//             patterns.push(&pattern[i .. j]);
//             j = j + 1;
//             i = j;
//         }
//     }
//
//     patterns
// }
//
// pub fn utf8_to_string(bytes: &[u8]) -> String {
//   let vector: Vec<u8> = Vec::from(bytes);
//   String::from_utf8(vector).unwrap()
// }
