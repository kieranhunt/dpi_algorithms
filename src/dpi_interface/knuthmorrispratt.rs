use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

const MAX_PATTERN_LENGTH: usize = 1000; //Just thumb-sucked this. It limits the size of the pattern.

pub struct KnuthMorrisPratt <'a> {
    pattern: &'a [u8],
    table: [isize; MAX_PATTERN_LENGTH],
}

impl <'a> KnuthMorrisPratt <'a> {
    fn knuthmorrispratt_table(pattern: &[u8]) -> [isize; MAX_PATTERN_LENGTH] {

        let pattern_len = pattern.len();

        let mut table  = [0; MAX_PATTERN_LENGTH];

        let mut pos: usize = 2;
        let mut cnd: usize = 0;

        table[0] = -1;
        table[1] = 0;

        while pos < pattern_len {
            if pattern[pos-1] == pattern[cnd] {
                cnd = cnd + 1;
                table[pos] = cnd as isize;
                pos = pos + 1;
            } else if cnd > 0 {
                cnd = table[cnd] as usize;
            } else {
                table[pos] = 0;
                pos = pos + 1;
            }
        }
        table
    }
}

impl <'a> Algorithm <'a> for KnuthMorrisPratt <'a> {
    fn new(ruleset: &'a Ruleset) -> KnuthMorrisPratt {
        KnuthMorrisPratt {
            pattern: ruleset.rules.as_bytes(),
            table: KnuthMorrisPratt::knuthmorrispratt_table(ruleset.rules.as_bytes()),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.pattern = ruleset.rules.as_bytes();
        self.table = KnuthMorrisPratt::knuthmorrispratt_table(self.pattern);
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let pattern_len = self.pattern.len();

        let mut m = 0;
        let mut i = 0;

        let mut partial_match = self.table;

        while m + i < packet_len {
            if self.pattern[i] == data[m + i] {
                if i == pattern_len - 1 {
                    inspection_result.results[0] = 1;
                    break;
                }
                i = i + 1;
            } else {
                if partial_match[i] > -1 {
                    m = m + i - partial_match[i] as usize;
                    i = partial_match[i] as usize;
                } else {
                    i = 0;
                    m = m + 1;
                }
            }
        }

        inspection_result
    }
}
