use std::cmp::max;
use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

const MAX_PATTERN_LENGTH: usize = 1000;
const ALPHABET_SIZE: usize = 256;

pub struct BoyerMoore <'a> {
    pattern: &'a [u8],
    character_table: [usize; ALPHABET_SIZE],
    offset_table: [isize; MAX_PATTERN_LENGTH],
}

impl <'a> BoyerMoore <'a> {
    fn is_prefix(pattern: &[u8], p: usize) -> bool {
        let pattern_len = pattern.len();

        let mut i = p;
        let mut j = 0;

        let mut prefix = true;

        loop {
            if i >= pattern_len {
                break;
            }
            if pattern[i] != pattern[j] {
                prefix = false;
                break;
            }

            i = i + 1;
            j = j + 1;
        }
        prefix
    }

    fn suffix_length(pattern: &[u8], p: isize) -> usize {
        let pattern_len = pattern.len();

        let mut length = 0;

        let mut i = p;
        let mut j = pattern_len - 1;

        loop {
            if i < 0 || pattern[i as usize] != pattern[j as usize] {
                break;
            }
            length = length + 1;
            i = i - 1;
            j = j - 1;
        }
        length
    }

    fn character_table(pattern: &[u8]) -> [usize; ALPHABET_SIZE] {
        let pattern_len = pattern.len();

        let mut table: [usize; ALPHABET_SIZE]  = [0; ALPHABET_SIZE];

        for i in 0..ALPHABET_SIZE {
            table[i] = pattern_len;
        }
        for i in 0..(pattern_len - 1) {
            let pattern_i = pattern[i] as usize;
            table[pattern_i] = pattern_len - 1 - i;
        }

        table
    }

    pub fn offset_table(pattern: &[u8]) -> [isize; MAX_PATTERN_LENGTH] {
        let mut table: [isize; MAX_PATTERN_LENGTH]  = [0; MAX_PATTERN_LENGTH];

        let pattern_len = pattern.len();

        let mut lastPrefixPosition = pattern_len;

        let mut j = pattern_len - 1;

        loop {
            if BoyerMoore::is_prefix(pattern, j + 1) {
                lastPrefixPosition = j + 1;
            }
            table[pattern_len - 1 - j] = (lastPrefixPosition - j + pattern_len - 1) as isize;

            if j == 0 {
                break;
            }

            j = j - 1;
        }

        for i in 0..(pattern_len - 1) {
            let suffix_length = BoyerMoore::suffix_length(pattern, i as isize);
            table[suffix_length] = (pattern_len - 1 - i + suffix_length) as isize;
        }

        table
    }
}

impl <'a> Algorithm <'a> for BoyerMoore <'a> {
    fn new(ruleset: &'a Ruleset) -> BoyerMoore {
        let pattern_array = ruleset.rules.as_bytes();
        BoyerMoore {
            pattern: pattern_array,
            character_table: BoyerMoore::character_table(pattern_array),
            offset_table: BoyerMoore::offset_table(pattern_array),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        let pattern_array = ruleset.rules.as_bytes();
        self.pattern = pattern_array;
        self.character_table = BoyerMoore::character_table(pattern_array);
        self.offset_table = BoyerMoore::offset_table(pattern_array);
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let pattern_len = self.pattern.len();

        if packet_len == 0 {
            return inspection_result;
        }

        let mut i = pattern_len - 1;
        let mut j = pattern_len - 1;

        'outer: loop {
            if (i >= packet_len) {
                break 'outer;
            }
            j = pattern_len - 1;
            'inner: loop {
                if (self.pattern[j] != data[i]) {
                    break 'inner;
                }
                if (j == 0) {
                    inspection_result.results[0] = 1;
                    break 'outer;
                }
                i = i - 1;
                j = j - 1;
            }
            // i = i + pattern_len - j; // <-- Naive method.
            let offset_table_result = self.offset_table[pattern_len - 1 - j];
            let character_table_result = self.character_table[data[i] as usize];
            i = i + max(offset_table_result, character_table_result as isize) as usize;
        }
        inspection_result
    }
}
