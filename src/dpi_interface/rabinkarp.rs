use dpi_interface::algorithm::Algorithm;
use ruleset::ruleset::Ruleset;
use ruleset::inspectionresult::InspectionResult;
use crypto::digest::Digest;
use crypto::md5::Md5;
use farmhash;

pub struct RabinKarp <'a> {
    pattern: &'a [u8],
}

impl <'a> Algorithm <'a> for RabinKarp <'a> {
    fn new(ruleset: &'a Ruleset) -> RabinKarp {
        RabinKarp {
            pattern: ruleset.rules.as_bytes(),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.pattern = ruleset.rules.as_bytes();
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let pattern_len = self.pattern.len();

        let pattern_hash = farmhash::hash64(self.pattern);

        let mut packet_hash = farmhash::hash64(&data[0 .. pattern_len]);

        for i in 0..(packet_len - pattern_len + 1) {
            if packet_hash == pattern_hash {
                inspection_result.results[0] = 1;
                break;
            }

            packet_hash = farmhash::hash64(&data[i .. i + pattern_len]);
        }
        inspection_result
    }
}
