use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

const ALPHABET_SIZE: usize = 256;

pub struct Horspool <'a> {
    pattern: &'a [u8],
    table: [usize; ALPHABET_SIZE],
}

impl <'a> Horspool <'a> {
    fn preprocess(pattern: &[u8]) -> [usize; ALPHABET_SIZE] {
        let pattern_len = pattern.len();
        let mut table = [pattern_len; ALPHABET_SIZE];

        for i in 0..(pattern_len - 1) {
            table[pattern[i] as usize] = pattern_len - 1 - i;
        }
        table
    }
}

impl <'a> Algorithm <'a> for Horspool <'a> {
    fn new(ruleset: &'a Ruleset) -> Horspool {
        let pattern = ruleset.rules.as_bytes();
        Horspool {
            pattern: pattern,
            table: Horspool::preprocess(pattern)
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        let pattern = ruleset.rules.as_bytes();
        self.pattern = pattern;
        self.table = Horspool::preprocess(pattern);
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let pattern_len = self.pattern.len();

        let mut skip = 0;

        while packet_len - skip >= pattern_len {
            let mut i = pattern_len - 1;
            while data[skip + i] == self.pattern[i] {
                if i == 0 {
                    inspection_result.results[0] = 1;
                    return inspection_result;
                }
                i = i - 1;
            }
            skip = skip + self.table[data[skip + pattern_len - 1] as usize];
        }

        inspection_result
    }
}
