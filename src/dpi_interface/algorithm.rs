use ruleset::ruleset::Ruleset;
use ruleset::inspectionresult::InspectionResult;

pub trait Algorithm <'a> {
    fn new(ruleset: &'a Ruleset) -> Self;
    fn change_ruleset(&mut self, ruleset: &'a Ruleset);
    fn inspect_data(&self, data: &[u8]) -> InspectionResult;
}
