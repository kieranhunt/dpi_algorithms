use pcap::Capture;

pub fn BPF(capture: &mut Capture, rules: &String){
    capture.filter(&rules[..]);
}
