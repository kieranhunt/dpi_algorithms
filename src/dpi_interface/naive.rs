use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

pub struct Naive <'a> {
    pattern: &'a [u8],
}

impl <'a> Algorithm <'a> for Naive <'a> {
    fn new(ruleset: &'a Ruleset) -> Naive {
        Naive {
            pattern: ruleset.rules.as_bytes(),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.pattern = ruleset.rules.as_bytes();
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let pattern_len = self.pattern.len();
        let packet_len = data.len();
        let mut inspection_result = InspectionResult::new(1);

        for n in 0..(packet_len - pattern_len + 1) {
            for i in 0..(pattern_len) {
                if data[n+i] != self.pattern[i] {
                    break;
                } else if i + 1 == pattern_len {
                    inspection_result.results[0] = 1;
                    break;
                }
            }
        }
        inspection_result
    }
}
