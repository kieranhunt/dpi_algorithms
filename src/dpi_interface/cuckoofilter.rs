use cuckoofilter::CuckooFilter;
use std::str;
use crypto::digest::Digest;
use crypto::md5::Md5;
use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;
use std::hash::{Hasher, SipHasher};

pub struct Cuckoo {
    cuckoo_filter: CuckooFilter<SipHasher>,
}

impl Cuckoo{
    fn make_filter(ruleset: &Ruleset) -> CuckooFilter<SipHasher> {
        let patterns = ruleset.rules.as_bytes();
        let patterns_vec = get_patterns(patterns);
        let mut cuckoo_filter = CuckooFilter::new();

        let mut md5 = Md5::new();

        for pattern in patterns_vec {
            md5.input(pattern);
            cuckoo_filter.add(&md5.result_str());
            md5.reset();
        }

        cuckoo_filter
    }
}

impl <'a> Algorithm <'a> for Cuckoo {
    fn new(ruleset: &'a Ruleset) -> Cuckoo{
        Cuckoo {
            cuckoo_filter: Cuckoo::make_filter(ruleset),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        self.cuckoo_filter = Cuckoo::make_filter(ruleset);
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {
        let mut inspection_result = InspectionResult::new(1);

        let packet_len = data.len();
        let packet = data;

        let mut md5 = Md5::new();

        for i in 0..packet_len {
            for j in i..packet_len {
                let packet_substring = &packet[i..j+1];
                md5.input(packet_substring);
                if self.cuckoo_filter.contains(&md5.result_str()) {
                        inspection_result.results[0] = 1;
                        return inspection_result;
                }
                md5.reset();
            }
        }
        inspection_result
    }
}




pub fn cuckoofilter(packet: &[u8], pattern_string: &String) -> bool {
    let patterns: &[u8] = pattern_string.as_bytes();
    let patterns_vec = get_patterns(patterns);
    let patterns_vec_len = patterns_vec.len();
    let mut cuckoo_filter = CuckooFilter::new();

    let packet_len = packet.len();

    let mut md5 = Md5::new();

    for pattern in patterns_vec {
        md5.input(pattern);
        cuckoo_filter.add(&md5.result_str());
        md5.reset();
    }

    for i in 0..packet_len {
        for j in i..packet_len {
            let packet_substring = &packet[i..j+1];
            md5.input(packet_substring);
            if cuckoo_filter.contains(&md5.result_str()) {
                    return true;
            }
            md5.reset();
        }
    }

    return false;
}

fn get_patterns(pattern: &[u8]) -> Vec<&[u8]> {

    const DELIMETER: u8 = 0x3b; //Delimeter is ';'

    let pattern_len = pattern.len();

    let mut patterns = Vec::new();

    let mut i = 0;

    for mut j in 0..pattern_len {
        if (pattern[j] == DELIMETER) {
            patterns.push(&pattern[i .. j]);
            j = j + 1;
            i = j;
        }
    }

    patterns
}

pub fn utf8_to_string(bytes: &[u8]) -> String {
  let vector: Vec<u8> = Vec::from(bytes);
  String::from_utf8(vector).unwrap()
}
