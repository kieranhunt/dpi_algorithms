use std::str;
use aho_corasick::{Automaton, AcAutomaton, Match};
use dpi_interface::algorithm::Algorithm;
use ruleset::inspectionresult::InspectionResult;
use ruleset::ruleset::Ruleset;

const DELIMETER: u8 = 0x3b; //Delimeter is ';'

pub struct AhoCorasick <'a>{
    ac_automaton: AcAutomaton<&'a str>,

}

impl <'a>AhoCorasick<'a> {
    fn ahocorasick(&self, packet: &[u8]) -> bool {
        let packet_str = match str::from_utf8(packet) {
            Ok(x) =>  x,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
        };
        let mut it = self.ac_automaton.find(packet_str);

        for matcher in 0..self.ac_automaton.len() {
            match it.next() {
                Some(x) => return true,
                None => {},
            }
        }

        return false
    }
    fn get_patterns(pattern: &[u8]) -> Vec<&str> {
        let pattern_len = pattern.len();
        let mut patterns = Vec::new();
        let mut i = 0;
        for mut j in 0..pattern_len {
            if (pattern[j] == DELIMETER) {
                patterns.push(&pattern[i .. j]);
                j = j + 1;
                i = j;
            }
        }
        let mut new_patterns = Vec::new();
        for new_pattern in patterns {
            new_patterns.push(match str::from_utf8(new_pattern) {
                Ok(x) =>  x,
                Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
            });
        }
        new_patterns
    }
}

impl <'a> Algorithm <'a> for AhoCorasick<'a> {
    fn new(ruleset: &'a Ruleset) -> AhoCorasick {
        let pattern_array: &[u8] = ruleset.rules.as_bytes();
        let patterns_vec = AhoCorasick::get_patterns(pattern_array);
        AhoCorasick{
            ac_automaton: AcAutomaton::new(patterns_vec),
        }
    }
    fn change_ruleset(&mut self, ruleset: &'a Ruleset) {
        let pattern_array: &[u8] = ruleset.rules.as_bytes();
        let patterns_vec = AhoCorasick::get_patterns(pattern_array);
        self.ac_automaton = AcAutomaton::new(patterns_vec);
    }
    fn inspect_data(&self, data: &[u8]) -> InspectionResult {

        let automaton_size = self.ac_automaton.len();

        let mut inspection_results = InspectionResult::new(automaton_size);

        let packet_str = match str::from_utf8(data) {
            Ok(x) =>  x,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
        };
        let mut it = self.ac_automaton.find_overlapping(packet_str);

        loop {
            let next = it.next();
            if (next == None) {
                break;
            }
            inspection_results.results[next.unwrap().pati] = 1;
        }

        inspection_results
    }
}
