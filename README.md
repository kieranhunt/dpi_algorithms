# DPI Algorithms

[![Build Status](https://travis-ci.org/KieranHunt/dpi_algorithms.svg)](https://travis-ci.org/KieranHunt/dpi_algorithms)

This library contains various algorithms for performing deep packet inspection.
All of the algorithms will implement a common interface and return the same objects.
This library has been created for the purpose of benchmarking various deep packet inspection algorithms.

## Algorithms

* [x] Naïve
* [x] [Rabin-Karp](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.86.9502&rep=rep1&type=pdf)
* [x] [Knuth-Morris-Pratt](https://www.cs.jhu.edu/~misha/ReadingSeminar/Papers/Knuth77.pdf)
* [x] [Boyer-Moore](http://delivery.acm.org/10.1145/360000/359859/p762-boyer.pdf)
* [x] [Bitap](http://delivery.acm.org/10.1145/140000/135243/p74-baeza-yates.pdf)
* [x] [Horspool (Boyer-Moore-Horspool)](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.63.3421&rep=rep1&type=pdf)
* [x] [Aho-Corasick](http://delivery.acm.org/10.1145/370000/360855/p333-aho.pdf) or [this](http://www.cs.uku.fi/~kilpelai/BSA05/lectures/slides04.pdf)
* [x] [Bloom Filter](https://www.cs.upc.edu/~diaz/p422-bloom.pdf)
* [x] [Cuckoo Filter](https://www.cs.cmu.edu/~dga/papers/cuckoo-conext2014.pdf)
* [ ] ~~[Berkeley Packet Filters (BNF)](http://tylerfisher.org/bpf/)~~
* [ ] ~~Parallel Bloom-filters~~
* [ ] ~~Setwise Horspool~~
